--removing previously inserted film

DELETE FROM inventory
WHERE film_id = 1001;

DELETE FROM rental
WHERE inventory_id IN (
    SELECT inventory_id
    FROM inventory
    WHERE film_id = 1001
);

--removing all data related to me as a customer
DELETE FROM payment p
WHERE (
	SELECT c.customer_id
	FROM customer c
	WHERE c.first_name = 'Farrukh') = p.customer_id;

DELETE FROM rental r
WHERE (
	SELECT c.customer_id
	FROM customer c
	WHERE c.first_name = 'Farrukh') = r.customer_id;