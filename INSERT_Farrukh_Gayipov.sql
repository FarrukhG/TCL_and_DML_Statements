--inserting a film
insert into film (title, description, release_year, language_id, rental_duration, length)

select 'The Raid 2', 
		'This is the description of the film',
		2014,
		l.language_id,
		2,
		150
from "language" l
where upper(l.name) = 'ENGLISH';

--inserting actor into actor table
insert into actor (first_name, last_name)
values ('Iko', 'Uwais'),
		('Arifin', 'Putra'),
		('Oka', 'Antara');

--inserting data ito film-actor table
insert into film_actor (actor_id, film_id)
values (201, 1001),
		(202, 1001),
		(203, 1001);