--updation film I added
UPDATE film
SET rental_duration = 3, 
	rental_rate = 9.99
WHERE film.film_id = 1001;


--finding customer with at least 10 rental and 10 payment records and changing the data into personal
SELECT c.customer_id, c.first_name, c.last_name
FROM customer c
WHERE (
    SELECT COUNT(*) 
    FROM rental r
    WHERE r.customer_id = c.customer_id
) >= 10
AND (
    SELECT COUNT(*) 
    FROM payment p
    WHERE p.customer_id = c.customer_id
) >= 10;

UPDATE customer
SET first_name = 'Farrukh',
    last_name = 'Gayipov',
    email = 'Farrukh@gayipov.com',
    address_id = 3,
    create_date = current_date
WHERE customer_id = 1;